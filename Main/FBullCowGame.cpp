#include "FBullCowGame.h"

FBullCowGame::FBullCowGame() {
	Reset();
}

int FBullCowGame::GetMaxTries() const {return MyMaxTries;}

int FBullCowGame::GetCurrentTry() const {return MyCurrentTry;}

bool FBullCowGame::IsGameWon() const {return false;}



void FBullCowGame::Reset() {
	constexpr int MAX_TRIES = 8;
	MyCurrentTry = MAX_TRIES;
	MyMaxTries = 8;
	return;
}

bool FBullCowGame::CheckGuessValidity(std::string guess) {
	return false;
}

void FBullCowGame::AddToList() {
}

void FBullCowGame::CheckGuessToList(std::string guess) {
}
