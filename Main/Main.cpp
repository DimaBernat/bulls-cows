//#include "Main.h"
#include <iostream>
#include <string>;
#include "FBullCowGame.h"

//using namespace std;

FBullCowGame BCGame;

void PrintIntro();
void PlayGame();
std::string GuessIsogram();
void PrintBack (std::string m_guessSTR);
constexpr int m_numberOfTurns = 5;

bool AskToPlayAgain();

int main() {
	bool ArePlayingAgain = false;
	do{
		PrintIntro();
		PlayGame();
		//TODO add a game summary
	} while (AskToPlayAgain());
	return 0;
}

 
void PrintIntro() { //Game Introducstion
	
	//constexpr int m_numberOfTurns = 5;
	std::cout << "Welcome to Bulls & Cows\n";
	std::cout << "Can you guess the " << m_numberOfTurns << " letter isogram I'm thinking of?\n";
	std::cout << std::endl;
}

void PlayGame() {  // loop for the number of turns
	BCGame.Reset();
	int m_maxTries = BCGame.GetMaxTries();
	std::cout  << "You have " << m_maxTries<< " Tries\n";
	for (size_t i = 0; i < m_numberOfTurns; i++){
		std::string m_guessSTR = GuessIsogram();
		PrintBack(m_guessSTR);
		std::cout << std::endl;
	}
}

std::string GuessIsogram() { //Get a guess from a player	
	int m_currentTry = BCGame.GetCurrentTry();
	std::string guess;
	std::cout << "Try " << m_currentTry <<". type your guess: ";
	std::getline(std::cin, guess); //TODO make a loop checking validastion

	//Submit Valid guess to the game
	//print number of bulls and cows
	return guess;
}

void PrintBack(std::string m_guessSTR)
{
	std::cout << "Your guess:" << m_guessSTR << std::endl;
}

bool AskToPlayAgain()
{
	std::cout << "Wanna Play again? (y/n)";
	std::string Response = "";
	std::getline(std::cin, Response);
	if (Response[0] == 'y' || Response[0] == 'Y')
	{			
		return true;
	}
	else
	{			
		return false;
	}
	//return Response[0];
}

